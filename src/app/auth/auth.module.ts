import { NgModule } from "@angular/core";
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { LoginComponent } from './login/login.component';
import { SignupComponent } from './signup/signup.component';
import { AuthRoutingModule } from './auth-routing.module';
import { MatCardModule } from '@angular/material/card';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatPaginatorModule } from '@angular/material/paginator';

@NgModule({
    declarations: [
        LoginComponent, 
        SignupComponent
    ],
    imports: [
        CommonModule, 
        FormsModule, 
        AuthRoutingModule,
        MatCardModule,
        MatInputModule,
        MatButtonModule,
        MatExpansionModule,
        MatToolbarModule,
        MatProgressSpinnerModule,
        MatPaginatorModule
    ]
})
export class AuthModule{}