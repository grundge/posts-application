import { Component, OnInit, EventEmitter, Output, OnDestroy } from '@angular/core';
import { Post } from '../post.model';
import { NgForm } from '@angular/forms';
import { PostService } from '../post.service';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { AuthService } from 'src/app/auth/auth.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-post-create',
  templateUrl: './post-create.component.html',
  styleUrls: ['./post-create.component.css']
})
export class PostCreateComponent implements OnInit, OnDestroy {

  isLoading = false;
  post: Post;
  private mode = 'create';
  private postId: string;
  private authStatusSub: Subscription;

  constructor(public postService: PostService, public route: ActivatedRoute, private authService: AuthService) { 
  }
  
  ngOnDestroy(): void {
    this.authStatusSub.unsubscribe();
  }

  ngOnInit() {
    this.authStatusSub = this.authService
    .getAuthStatusListener()
    .subscribe(authStatus => {
      this.isLoading = false;
    })
    this.route.paramMap.subscribe((paramMap: ParamMap) => {
      if(paramMap.has('postId')) {
        this.mode = 'edit';
        this.postId = paramMap.get('postId');
        this.isLoading = true;
        this.postService.getPost(this.postId).subscribe(postData => {
          this.isLoading = false;
          this.post = {id: postData._id, title: postData.title, content: postData.content};
        });
      }
      else {
        this.mode = 'create';
        this.postId = null;
      }
    })
  }

  onSavePost(form: NgForm){
    if(form.invalid){
      return;
    }

    const post: Post = {
      id: null,
      title: form.value.title,
      content: form.value.content,
    };

    this.isLoading = true;
    if(this.mode === 'create'){
      this.postService.addPost(post.title, post.content);
    } else {
      this.postService.updatePost(
        this.postId,
        post.title,
        post.content
      );
    }

    form.resetForm();
  }

}
