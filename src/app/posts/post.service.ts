import { Post } from './post.model';
import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Router } from '@angular/router';

@Injectable({providedIn: 'root'})
export class PostService{
    private posts: Post[] = [];
    private postsUpdated = new Subject<{posts: Post[], postCount: number}>();

    constructor(private httpClient: HttpClient, private router: Router){}

    getPost(postId: string) {
      return this.httpClient.get<{_id: string, title: string, content: string}>("http://localhost:3000/api/posts/"+postId);
    }

    getPosts(postsPerPage: number, currentPage: number) {
        const queryParams = `?pageSize=${postsPerPage}&page=${currentPage}`;
        this.httpClient.get<{message:string, posts: any, maxPosts: number}>(
          'http://localhost:3000/api/posts' + queryParams)
        .pipe(
            map(postData => {
              return {
                posts: postData.posts.map(post => {
                  return {
                    title: post.title,
                    content: post.content,
                    id: post._id
                  };
                }),
                maxPosts: postData.maxPosts
              };
            })
          )
        .subscribe((postData) => {
            this.posts = postData.posts;
            this.postsUpdated.next({posts: [...this.posts], postCount: postData.maxPosts});
        });
    }

    getPostUpdateListener() {
        return this.postsUpdated.asObservable();
    }

    addPost(title: string, content: string) {
        const post: Post = {
            id: null,
            title: title,
            content: content
        }
        this.httpClient
        .post<{ message: string, postId: string }>('http://localhost:3000/api/posts', post)
        .subscribe((responseData) => {
            this.router.navigate(["/"]);
        });
    }

    updatePost(id: string, title: string, content: string) {
      const post: Post = { id: id, title: title, content: content };
      this.httpClient
      .put("http://localhost:3000/api/posts/" + id, post)
      .subscribe(response => {
        this.router.navigate(["/"]);
      });
    }

    deletePost(postId: string) {
        return this.httpClient
        .delete<{message: string}>('http://localhost:3000/api/posts/' + postId);
    }
}