const express = require('express');
const router = express.Router();

const PostController = require("../controllers/post");

router.post("", PostController.createPost);
router.get("/:id", PostController.getPost);
router.put("/:id", PostController.editPost);
router.get("", PostController.getAllPosts);
router.delete("/:id", PostController.deletePost);

module.exports = router;