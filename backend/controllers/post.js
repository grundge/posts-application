const Post = require("../models/post");

exports.createPost = (req, res, next) => {
    const post = new Post({
        title: req.body.title,
        content: req.body.content
    });
    //sequelize
    //Post.create({ title: req.body.title, content: req.body.content})
    //post.then((createdPost) => {
    post
    .save()
    .then( createdPost => {
        res.status(201).json({
            message: 'Post criado com sucesso',
            postId: createdPost._id
        })
    })
    .catch((err) => {
        res.status(500).json({
            message: 'Erro ao criar post'
        })
    });
}

exports.getPost = (req, res, next) => {
    Post.findById(req.params.id)
    .then(post => {
        if(post) {
            res.status(200).json(post);
        } else {
            res.status(404).json({message: "post não encontrado"});
        }
    })
    .catch(error => {
        console.log(error);
        res.status(500).json({
            message: "Falhou a procura de um post"
        });
    });
}

exports.editPost = (req, res, next) => {
    const post = new Post({
        _id: req.body.id,
        title: req.body.title,
        content: req.body.content
    })
    Post.updateOne({ _id: req.params.id }, post)
    .then(result => {
        if(result.n > 0) {
            res.status(200).json({message: "update com sucesso"})
        } else {
            res.status(404).json({message: "Não foi possível atualizar"})
        }
    })
    .catch(error => {
        res.status(500).json({
            message: 'Erro ao atualizar post'
        })
    });
}

exports.getAllPosts = (req, res, next) => {
    const pageSize = +req.query.pageSize;
    const currentPage = +req.query.page;
    const postQuery = Post.find();
    console.log(postQuery);
    let fetchedPosts;
    //sequelize
    //Post.findAll({ raw: true }).then((posts) => {
    if(pageSize && currentPage) {
        postQuery.skip(pageSize * (currentPage - 1)).limit(pageSize);
    }
    postQuery
    .then(posts => {
        fetchedPosts = posts;
        return Post.count();
    })
    .then(count => {
        res.status(200).json({
            message: "Posts enviados com sucesso",
            posts: fetchedPosts,
            maxPosts: count
        });
    })
    .catch(error => {
        console.log(error);
        res.status(500).json({
            message: 'Erro ao fazer fetch de all posts'
        })
    });;
}

exports.deletePost = (req, res, next) => {
    //sequelize
    //Post.destroy({ where: { _id: req.params.id} })
    Post.deleteOne({ _id: req.params.id })
    .then((result) => {
        res.status(200).json({message: "Post deleted"});
    })
}