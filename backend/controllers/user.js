const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
require('dotenv').config();
const User = require("../models/user");

exports.signup = (req, res, next) => {
    bcrypt.hash(req.body.password, 10)
    .then(hash => {
        const user = new User({
            email: req.body.email,
            password: hash
        });
        user
        .save()
        .then(result => {
                res.status(201).json({
                    message: "User created!",
                    result: result
                });
            }
        )
        .catch(error => {
            return res.status(401).send("Auth failed");
        })
    })
    .catch(error => {
        console.log(error);
        return res.status(401).send("Auth failed");
    })
}

exports.login = (req, res, next) => {
    let fetchedUser;
    // procurar apenas 1 resultado pelo email
    User.findOne({ email: req.body.email})
    .then( user => {
        if(!user) {
            return res.status(401).send("Auth failed");
        }
        fetchedUser = user;
        return bcrypt.compare(req.body.password, user.password);
    })
    .then(result => {
        if(!result) {
            return res.status(401).send("Auth failed");
        }
        const token = jwt.sign(
            { email: fetchedUser.email, userId: fetchedUser._id },
            "Barnett76Wires10Newsreel",
            { expiresIn: "1h" }
        );
        res.status(201).json({
            token: token,
            expiresIn: 3600,
            userId: fetchedUser._id
        });
    })
    .catch(error => {
        return res.status(401).send("Auth failed");
    })
}