const express = require('express');
const app = express();
const bodyParser = require("body-parser");
const mongoose = require('mongoose');

// sequelize model
//const { Post } = require("./database/sequelize");
// mongoose model
const postsRoutes = require("./routes/posts");
const userRoutes = require("./routes/user");

const Post = require("./models/post");

mongoose
    .connect(
        "mongodb+srv://postsUser:tt58WeKLzfxD748A@cluster0-vc5bb.mongodb.net/test?retryWrites=true&w=majority"
    )
    .then(() => {
        console.log("Connected to database");
    })
    .catch((error) => {
        console.log("Connection failed!: "+error);
    })

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));


app.use((req, res, next) => {
    res.setHeader("Access-Control-Allow-Origin", "*");
    res.setHeader("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization");
    res.setHeader("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS");
    next();
});

app.use("/api/posts", postsRoutes);
app.use("/api/user", userRoutes);

module.exports = app;